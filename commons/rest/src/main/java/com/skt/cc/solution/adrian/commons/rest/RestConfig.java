package com.skt.cc.solution.adrian.commons.rest;

import com.skt.cc.solution.adrian.commons.rest.implementation.RestImplementation;
import com.skt.cc.solution.adrian.commons.rest.infra.marshalling.MarshallingConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({RestImplementation.class, MarshallingConfig.class})
public class RestConfig {
}
