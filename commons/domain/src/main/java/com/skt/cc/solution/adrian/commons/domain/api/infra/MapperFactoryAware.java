package com.skt.cc.solution.adrian.commons.domain.api.infra;

import ma.glasnost.orika.MapperFactory;

public interface MapperFactoryAware {
    void setMapperFactory(MapperFactory mapperFactory);
}
