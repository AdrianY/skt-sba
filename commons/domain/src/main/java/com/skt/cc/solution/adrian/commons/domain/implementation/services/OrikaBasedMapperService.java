package com.skt.cc.solution.adrian.commons.domain.implementation.services;

import com.skt.cc.solution.adrian.commons.domain.api.services.MapperService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrikaBasedMapperService implements MapperService {

    private final MapperFacade mapperFacade;

    public OrikaBasedMapperService(MapperFactory mapperFactory) {
        this.mapperFacade = mapperFactory.getMapperFacade();
    }

    @Override
    public <TARGET, SOURCE> List<TARGET> map(List<SOURCE> sources, Class<TARGET> targetClass) {
        if(CollectionUtils.isEmpty(sources))
            return Collections.emptyList();

        return sources.stream()
                .map(source -> mapperFacade.map(source, targetClass))
                .collect(Collectors.toList());
    }

    @Override
    public <TARGET, SOURCE> TARGET map(SOURCE source, Class<TARGET> targetClass) {
        return mapperFacade.map(source, targetClass);
    }
}
