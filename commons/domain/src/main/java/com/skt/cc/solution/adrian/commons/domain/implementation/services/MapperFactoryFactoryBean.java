package com.skt.cc.solution.adrian.commons.domain.implementation.services;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.stereotype.Component;

@Component
public class MapperFactoryFactoryBean extends AbstractFactoryBean<MapperFactory> {

    @Override
    public Class<?> getObjectType() {
        return MapperFactory.class;
    }

    @Override
    protected MapperFactory createInstance() {
        return new DefaultMapperFactory.Builder().build();
    }
}
