package com.skt.cc.solution.adrian.commons.domain.infra;

import com.skt.cc.solution.adrian.commons.domain.api.services.MapperService;
import com.skt.cc.solution.adrian.commons.domain.api.services.MapperServiceAware;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MapperServiceAwareBeanPostProcessor implements BeanPostProcessor {

    private final MapperService mapperService;

    public MapperServiceAwareBeanPostProcessor(MapperService mapperService) {
        this.mapperService = mapperService;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof MapperServiceAware){
            MapperServiceAware mapperServiceAware = (MapperServiceAware) bean;
            mapperServiceAware.setMapperService(mapperService);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
