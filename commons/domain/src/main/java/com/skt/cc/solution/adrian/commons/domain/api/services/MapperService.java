package com.skt.cc.solution.adrian.commons.domain.api.services;

import java.util.List;

public interface MapperService {
    <TARGET, SOURCE> List<TARGET> map(List<SOURCE> sourceList, Class<TARGET> targetClass);

    <TARGET, SOURCE> TARGET map(SOURCE source, Class<TARGET> targetClass);
}
