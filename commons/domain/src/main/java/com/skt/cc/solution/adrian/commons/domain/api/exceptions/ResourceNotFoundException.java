package com.skt.cc.solution.adrian.commons.domain.api.exceptions;

public class ResourceNotFoundException extends RuntimeException{
    private final Object identifier;

    public ResourceNotFoundException(Object identifier) {
        super();
        this.identifier = identifier;
    }

    public Object getIdentifier() {
        return identifier;
    }
}
