package com.skt.cc.solution.adrian.commons.domain.api.dto;

public class BaseDTO {
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
