package com.skt.cc.solution.adrian.commons.domain.api.services;

import java.util.List;

public interface ResourceMaintenanceService<T> {

    List<T> findAll(String searchString);

    List<T> findAll();

    T create(T resource);

    void remove(Long resourceId);
}
