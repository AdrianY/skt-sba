package com.skt.cc.solution.adrian.commons.domain.api.services;

public interface MapperServiceAware {
    void setMapperService(MapperService mapperService);
}
