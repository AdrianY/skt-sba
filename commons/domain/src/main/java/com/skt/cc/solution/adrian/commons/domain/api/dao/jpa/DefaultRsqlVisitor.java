package com.skt.cc.solution.adrian.commons.domain.api.dao.jpa;

import cz.jirutka.rsql.parser.ast.AndNode;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.NoArgRSQLVisitorAdapter;
import cz.jirutka.rsql.parser.ast.OrNode;
import org.springframework.data.jpa.domain.Specification;

public class DefaultRsqlVisitor<T> extends NoArgRSQLVisitorAdapter<Specification<T>> {

    private final GenericRsqlSpecBuilder<T> builder;

    public DefaultRsqlVisitor() {
        this.builder = new GenericRsqlSpecBuilder<>();
    }

    @Override
    public Specification<T> visit(AndNode andNode) {
        return builder.createSpecification(andNode);
    }

    @Override
    public Specification<T> visit(OrNode orNode) {
        return builder.createSpecification(orNode);
    }

    @Override
    public Specification<T> visit(ComparisonNode comparisonNode) {
        return builder.createSpecification(comparisonNode);
    }
}
