package com.skt.cc.solution.adrian.commons.domain.api.dao.jpa;

import com.skt.cc.solution.adrian.commons.domain.api.entities.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BaseJpaDao<T extends BaseEntity> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {
}
