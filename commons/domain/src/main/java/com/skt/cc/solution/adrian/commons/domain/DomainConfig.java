package com.skt.cc.solution.adrian.commons.domain;

import com.skt.cc.solution.adrian.commons.domain.implementation.DomainImplementation;
import com.skt.cc.solution.adrian.commons.domain.infra.DomainProcessors;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        DomainImplementation.class,
        DomainProcessors.class
})
public class DomainConfig {
}
